import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';
import api from '../../services/api';
import {
  Container,
  ContainerButton,
  ImageProduct,
  PriceProduct,
  TitleProduct,
  ContainerProduct,
  ContainerProductAmount,
  ProductAmount,
  ButtonText,
  List,
} from './styles';

import * as CartActions from '../../store/modules/Cart/actions';

function Main({ navigation, addToCartRequest, amount }) {
  const [products, setProducts] = useState([]);

  async function getProducts() {
    const response = await api.get('/products');
    setProducts(response.data);
  }

  useEffect(() => {
    getProducts();
  }, []);

  function handleAddproduct(id) {
    // navigation.navigate('Cart');
    addToCartRequest(id);
  }

  return (
    <Container>
      <List
        data={products}
        keyExtractor={(product) => String(product.id)}
        renderItem={({ item }) => (
          <ContainerProduct>
            <ImageProduct source={{ uri: item.image }} />
            <TitleProduct>{item.title}</TitleProduct>
            <PriceProduct>{item.price}</PriceProduct>
            <ContainerButton onPress={() => handleAddproduct(item.id)}>
              <ContainerProductAmount>
                <Icon name="add-shopping-cart" size={20} color="#fff" />
                <ProductAmount>{amount[item.id] || 0}</ProductAmount>
              </ContainerProductAmount>
              <ButtonText>ADICIONAR</ButtonText>
            </ContainerButton>
          </ContainerProduct>
        )}
      />
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => bindActionCreators(CartActions, dispatch);

const mapStateToProps = (state) => ({
  amount: state.cart.reduce((amount, product) => {
    amount[product.id] = product.amount;
    return amount;
  }, {}),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
