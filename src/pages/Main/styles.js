import styled from 'styled-components/native';
import { darken } from 'polished';
import { RectButton } from 'react-native-gesture-handler';
import colors from '../../styles/colors';

export const Container = styled.View`
  flex: 1;
  padding: 0px 20px;
  background: ${colors.dark};
`;

export const ContainerProduct = styled.View`
  padding: 20px;
  align-items: center;
  background: white;
  border-radius: 6px;
  margin: 8px;
`;

export const ImageProduct = styled.Image`
  width: 124px;
  height: 124px;
  background: #999;
`;

export const TitleProduct = styled.Text`
  text-align: center;
  font-size: 14px;
`;

export const PriceProduct = styled.Text`
  margin: 5px;
  font-size: 18px;
  font-weight: bold;
  align-items: center;
  justify-content: center;
`;

export const ContainerButton = styled(RectButton)`
  flex-direction: row;
  background: ${colors.primary};
  align-items: center;
  justify-content: center;
  border-radius: 6px;
`;

export const ContainerProductAmount = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 8px;
  background: ${darken(0.1, colors.primary)};
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
`;

export const ProductAmount = styled.Text`
  margin-left: 4px;
`;

export const ButtonText = styled.Text`
  flex: 1;
  text-align: center;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})``;
