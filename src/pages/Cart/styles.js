import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import colors from '../../styles/colors';

export const Container = styled.View`
  flex: 1;
  background: ${colors.dark};
  padding: 0 20px;
`;

export const CartContainer = styled.View`
  padding: 20px;
  background: white;
  border-radius: 6px;
  margin: 8px;
`;

export const List = styled.FlatList`
  max-height: 300px;
`;

export const RowProduct = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ProductImage = styled.Image`
  width: 72;
  height: 72px;
  background: #999;
`;

export const InfoContainer = styled.View`
  flex: 1;
  flex-direction: column;
  padding: 0 4px;
`;

export const ProductTitle = styled.Text``;
export const ProductPrice = styled.Text`
  font-weight: bold;
`;

export const Trash = styled(RectButton)`
  flex-direction: column;
  padding: 4px;
  height: 24px;
  background: #7159c1;
  align-items: center;
  justify-content: center;
  border-radius: 6px;
`;

export const TextRemove = styled.Text`
  font-weight: bold;
  color: #fff;
`;

export const TextTotal = styled.Text`
  font-weight: bold;
  font-size: 16px;
`;

export const RowQuantity = styled.View`
  flex-direction: row;
  background: #999;
  align-items: center;
  justify-content: space-between;
  padding: 8px 8px;
`;

export const QuantityContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const AddButton = styled(RectButton)`
  width: 16px;
  height: 16px;
  border-radius: 8px;
  background: ${colors.primary};
  justify-content: center;
`;

export const SubButton = styled(RectButton)`
  width: 16px;
  height: 16px;
  border-radius: 8px;
  background: ${colors.primary};
  justify-content: center;
`;

export const DivQuatity = styled.View`
  background: #fff;
  border-radius: 6px;
  padding: 4px;
  margin: 0 4px;
  width: 24px;
  height: 24px;

  align-items: center;
  justify-content: center;
`;

export const TextQuantity = styled.Text`
  font-weight: bold;
`;

export const RowTotal = styled.View`
  padding: 12px;
  align-items: center;
`;

export const TextAllTotal = styled.Text`
  font-size: 24px;
  font-weight: bold;
`;

export const ButtonFinish = styled(RectButton)`
  height: 32px;
  background: #7159c1;
  border-radius: 6px;
  align-items: center;
  justify-content: center;
`;

export const TextFinish = styled.Text`
  font-weight: bold;
  color: #fff;
`;

export const TextTotalDecoration = styled.Text`
  font-weight: bold;
  color: #666;
`;

export const TextAdd = styled.Text`
  text-align: center;
  font-weight: bold;
  color: white;
`;

export const TextSub = styled.Text`
  text-align: center;
  font-weight: bold;
  color: white;
`;
