import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  Container,
  CartContainer,
  RowQuantity,
  RowProduct,
  ProductImage,
  ProductTitle,
  TextTotal,
  InfoContainer,
  QuantityContainer,
  ProductPrice,
  Trash,
  AddButton,
  SubButton,
  DivQuatity,
  TextRemove,
  RowTotal,
  ButtonFinish,
  TextAllTotal,
  TextFinish,
  TextTotalDecoration,
  TextQuantity,
  List,
  TextAdd,
  TextSub,
} from './styles';

import * as CartActions from '../../store/modules/Cart/actions';

function Cart({ cart, total, removeFromCart, updateAmountRequest }) {
  function handleRemoveProduct(id) {
    removeFromCart(id);
  }

  function increment(product) {
    updateAmountRequest(product.id, product.amount + 1);
  }
  function decrement(product) {
    updateAmountRequest(product.id, product.amount - 1);
  }
  return (
    <Container>
      <CartContainer>
        {cart.length ? (
          <>
            <List
              data={cart}
              keyExtractor={(product) => String(product.id)}
              renderItem={({ item }) => (
                <>
                  <RowProduct>
                    <ProductImage
                      source={{ uri: item.image }}
                      alt={item.title}
                    />
                    <InfoContainer>
                      <ProductTitle>{item.title}</ProductTitle>
                      <ProductPrice>{item.price}</ProductPrice>
                    </InfoContainer>
                    <Trash onPress={() => handleRemoveProduct(item.id)}>
                      <TextRemove>REMOVER</TextRemove>
                    </Trash>
                  </RowProduct>
                  <RowQuantity>
                    <QuantityContainer>
                      <AddButton onPress={() => increment(item)}>
                        <TextAdd>+</TextAdd>
                      </AddButton>
                      <DivQuatity>
                        <TextQuantity>{item.amount}</TextQuantity>
                      </DivQuatity>
                      <SubButton onPress={() => decrement(item)}>
                        <TextSub>-</TextSub>
                      </SubButton>
                    </QuantityContainer>
                    <TextTotal>{item.quantityValue}</TextTotal>
                  </RowQuantity>
                </>
              )}
            />
            <RowTotal>
              <TextTotalDecoration>Total</TextTotalDecoration>
              <TextAllTotal>{total}</TextAllTotal>
            </RowTotal>
            <ButtonFinish>
              <TextFinish>FINALIZAR COMPRA</TextFinish>
            </ButtonFinish>
          </>
        ) : (
          <View>
            <Text>Não há compras no carrinho</Text>
          </View>
        )}
      </CartContainer>
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => bindActionCreators(CartActions, dispatch);

const mapStateToProps = (state) => ({
  cart: state.cart.map((product) => ({
    ...product,
    quantityValue: product.amount * product.price,
  })),
  total: state.cart.reduce(
    (total, product) => product.price * product.amount + total,
    0
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
