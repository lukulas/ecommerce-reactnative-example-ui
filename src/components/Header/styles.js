import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native';

import colors from '../../styles/colors';
import logo from '../../assets/logo.png';

export const Container = styled.View`
  flex-direction: row;
  background: ${colors.dark};
  align-items: center;
  justify-content: space-between;
  padding: 4px 4px;
`;

export const ButtonCart = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;

  border-radius: 6px;
`;

export const ButtonLogo = styled(RectButton)``;

export const Logo = styled.Image.attrs({
  source: logo,
  resizeMode: 'cover',
})`
  width: 185px;
  height: 24px;
`;

export const DivQuantity = styled.View`
  position: absolute;
  top: -4px;
  right: 18px;
  background: green;
  height: 16px;
  width: 16px;
  border-radius: 6px;

  align-items: center;
  justify-content: center;
  overflow: hidden;
  border: 1px solid black;
`;

export const TextQuantity = styled.Text`
  font-weight: bold;
`;
