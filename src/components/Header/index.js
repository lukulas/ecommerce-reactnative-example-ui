import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';

import {
  Container,
  ButtonLogo,
  Logo,
  ButtonCart,
  DivQuantity,
  TextQuantity,
} from './styles';

Icon.loadFont();

const Header = ({ data, cart }) => (
  <Container>
    <ButtonLogo
      onPress={() => data.navigation.canGoBack() && data.navigation.goBack()}
    >
      <Logo />
    </ButtonLogo>

    <ButtonCart onPress={() => data.navigation.navigate('Cart')}>
      <DivQuantity>
        <TextQuantity>{cart}</TextQuantity>
      </DivQuantity>
      <Icon name="shopping-basket" size={24} color="#FFF" />
    </ButtonCart>
  </Container>
);

export default connect(({ cart }) => ({ cart: cart.length }))(Header);
