import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Header from './components/Header';
import Main from './pages/Main';
import Cart from './pages/Cart';

const Stack = createStackNavigator();

const Routes = () => (
  <Stack.Navigator
    initialRouteName="Main"
    screenOptions={{
      header: (data) => <Header data={data} />,
    }}
  >
    <Stack.Screen name="Main" component={Main} options={{ title: 'Main' }} />
    <Stack.Screen name="Cart" component={Cart} options={{ title: 'Cart' }} />
  </Stack.Navigator>
);

export default Routes;
