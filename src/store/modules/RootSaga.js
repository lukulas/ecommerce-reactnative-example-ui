import { all } from 'redux-saga/effects';

import cart from './Cart/sagas';

export default function* rooSaga() {
  return yield all([cart]);
}
