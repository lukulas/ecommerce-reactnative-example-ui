import { call, select, put, all, takeLatest } from 'redux-saga/effects';
import { Alert } from 'react-native';
import api from '../../../services/api';
import { addToCartSuccess, updateAmountSuccess } from './actions';
import * as Navigation from '../../../services/navigation';

function* addToCart({ id }) {
  const productExists = yield select((state) => state.cart.find((p) => p.id === id),
  );

  const stock = yield call(api.get, `/stock/${id}`);

  const stockAmount = stock.data.amount;
  const currentAmount = productExists ? productExists.amount : 0;

  const amount = currentAmount + 1;

  if (amount > stockAmount) {
    Alert.alert('Stock maximo');
    return;
  }

  if (productExists) {
    yield put(updateAmountSuccess(productExists.id, productExists.amount + 1));
  } else {
    const response = yield call(api.get, `/products/${id}`);

    const product = { ...response.data, amount: 1 };

    yield put(addToCartSuccess(product));

    Navigation.navigate('Cart');
  }
}

function* updateAmount({ id, amount }) {
  if (amount <= 0) return;

  const stock = yield call(api.get, `/stock/${id}`);
  const stockAmount = stock.data.amount;

  if (amount > stockAmount) {
    Alert.alert('Stock maximo.');
    return;
  }

  yield put(updateAmountSuccess(id, amount));
}

export default all([
  takeLatest('@cart/ADD_REQUEST', addToCart),
  takeLatest('@cart/UPDATE_AMOUNT_REQUEST', updateAmount),
]);
