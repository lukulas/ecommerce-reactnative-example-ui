import 'react-native-gesture-handler';
import React from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';

import Routes from './routes';
import './config/ReactotronConfig';
import store from './store';
import { navigationRef } from './services/navigation';

const App = () => (
  <Provider store={store}>
    <NavigationContainer ref={navigationRef}>
      <StatusBar />
      <Routes />
    </NavigationContainer>
  </Provider>
);

export default App;
